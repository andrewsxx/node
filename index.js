var express = require('express');
var app = express();
var connector = require("./connector");

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/topActiveUsers', function(req, res) {
    var url = require('url');
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    
    var start = 0;
    var limit = 50;
    if(query.page) start = query.page*50;
    
    var connector = require("./connector");
    connector.pool.connect(function(err, client, done) {
        if (err) {
            res.json({"code" : 100, "status" : "Error in connection database"});
        }
        
        var q = 'SELECT users.id, users.created_at, users.name, listings.name as lname, (SELECT count(*) FROM applications where user_id = users.id) as count FROM (SELECT * FROM users LIMIT ' + limit + ' OFFSET ' + start + ') as users LEFT JOIN listings ON users.id = listings.created_by';
        client.query(q, function(err, result) {
            done();
            var response = [];
            var id = null;
            var user = null;
            result.rows.forEach(function(entry) {
                if(id != entry.id && user) response.push(user);
                if(id != entry.id){
                    user = {};
                    user.id = entry.id;
                    user.created_at = entry.created_at;
                    user.name = entry.name;
                    user.listings = [];
                    id = entry.id;
                }
                if(entry.lname != null) user.listings.push(entry.lname);
                
            });
            if(result.rows.length == 1 && user) response.push(user);
            res.json(response);
        });
    });
});


app.get('/users', function(req, res) {
    var url = require('url');
    var url_parts = url.parse(req.url, true);
    var id = url_parts.query.id;
    var connector = require("./connector");
    var uh = new userHandler();
    connector.pool.connect(function(err, client, done) {
        if (err) {
            res.json({"code" : 100, "status" : "Error in connection database"});
        }
//        res.json({id : id});
        var q1 = 'SELECT * FROM users WHERE id = ' + id + ";";
        client.query(q1, function(err, result) {
            done();
            console.info(1);
            var rows = [];
            if(!err) rows = result.rows;
            uh.done(rows, "user", res);
        });
        
        var q2 = 'SELECT * FROM companies INNER JOIN teams on teams.company_id = companies.id AND teams.user_id = ' + id + ";";
        client.query(q2, function(err, result) {
            done();
            console.info(2);
            var rows = [];
            if(!err) rows = result.rows;
            uh.done(rows, "companies", res);
        });
        
        var q3 = 'SELECT * FROM listings where created_by = ' + id + ";";
        client.query(q3, function(err, result) {
            console.info(3);
            var rows = [];
            if(!err) rows = result.rows;
            done();
            uh.done(rows, "listings", res);
        });
        
        var q4 = 'SELECT applications.*, listings.id as lid, listings.name as lname, listings.description as description FROM applications INNER JOIN listings on listings.id = applications.listing_id where user_id = ' + id + ";";
        client.query(q4, function(err, result) {
            console.info(4);
            var rows = [];
            if(!err) rows = result.rows;
            done();
            uh.done(rows, "applications", res);
        });
    });
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

    var userHandler = function(){
        this.totalQueries = 4;
        this.numQueries = 0;
        this.object = {};
        this.responses = {};
    };
    
    userHandler.prototype.prepareObject = function(){
            var tthis = this;

            this.object['id'] = this.responses["user"][0].id;
            this.object['name'] = this.responses["user"][0].name;
            this.object['created_at'] = this.responses["user"][0].created_at;
            this.object['companies'] = [];
            this.responses["companies"].forEach(function(entry) {
                var company = {};
                company.id = entry.id;
                company.createdAt = entry.created_at;
                company.name = entry.name;
                company.isContact = entry.contact_user;
                tthis.object['companies'].push(company);
            });
            
            this.object['createdListings'] = [];
            this.responses["listings"].forEach(function(entry) {
                var listing = {};
                listing.id = entry.id;
                listing.createdAt = entry.created_at;
                listing.name = entry.name;
                listing.description = entry.description;
                tthis.object['createdListings'].push(listing);
            });
            
            var appId = null;
            var applications = [];
            var application = null;
            this.responses["applications"].forEach(function(entry) {
                if(appId != entry.id && application) applications.push(application);
                if(appId != entry.id){
                    application = {};
                    application.id = entry.id;
                    application.createdAt = entry.created_at;
                    application.name = entry.name;
                    application.coverLetter = entry.cover_letter;
                    application.listings = [];
                    appId = entry.id;
                }
                var listing = {};
                listing.id = entry.id;
                listing.name = entry.name;
                listing.description = entry.description;
                application.listings.push(listing);
            });
            
            if(this.responses["applications"].length == 1 && application) applications.push(application);
            
            tthis.object['applications'] = applications;
            
//        for(var key in this.responses){
//            if(key == "user"){
//            } else if(key == "companies"){
//            } else if(key == "listings"){
//            } else {
//                this.object[key] = this.responses[key];
//            }
//        }
    },
    
    userHandler.prototype.done = function(response, name, res){
        this.responses[name] = response;
        this.numQueries++;
        if(this.numQueries == this.totalQueries){
            this.prepareObject();
            this.run(res);
        }
    },
    userHandler.prototype.run = function(res){
        res.json(this.object);
    }